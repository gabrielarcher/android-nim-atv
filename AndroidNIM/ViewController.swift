//
//  ViewController.swift
//  AndroidNIM
//
//  Created by Gabriel Archer on 25/09/2015.
//  Copyright © 2015 swGuru Ltd. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet var shooters: [UIImageView]?
    @IBOutlet var robots0: [UIButton]?
    @IBOutlet var robots1: [UIButton]?
    @IBOutlet var robots2: [UIButton]?
    @IBOutlet var helpLabel: UILabel?
    @IBOutlet var laserView: UIImageView?

    @IBOutlet var resultView: UIVisualEffectView?
    @IBOutlet var resultLabel1: UILabel?
    @IBOutlet var resultLabel2: UILabel?

    var menuController: MenuController?
    
    
    var robots = [[UIButton]]()
    var redAnimBase = [UIImage]()
    var shooterAnimBase = [UIImage]()
    var redAnimBumm = [UIImage]()
    var shooterAnimBumm = [UIImage]()
    var shooterAnimDown = [UIImage]()
    var shooterAnimShortBumm = [UIImage]()
    var shooterAnimShortDown = [UIImage]()
    
    var isSecondPlayer = false
    var isSecondTurns = false
    var player: AVAudioPlayer?
    var mincols = [0, 0, 0]
    var counter = [0, 0, 0]
    var path = Path()
    var isLastShoot = false
    var isFirstShoot = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        robots.append(robots0!)
        robots.append(robots1!)
        robots.append(robots2!)
        
        //Init player
        let filePath = NSBundle.mainBundle().pathForResource("laser", ofType: "mp3")
        let url = NSURL(fileURLWithPath: filePath!)
        do {
            player = try AVAudioPlayer(contentsOfURL: url)
            player?.prepareToPlay()
        } catch let error {
            print(error)
        }
        
        setBaseAnim()
        
        for robotLine in robots {
            for robot in robotLine {
                robot.hidden=false;
                let val: Double = Double(arc4random_uniform(400)) / 400 + 0.6
                robot.imageView?.animationDuration=val
                robot.imageView?.animationRepeatCount=0
                robot.imageView?.animationImages=redAnimBase
                robot.imageView?.startAnimating()
            }
        }

        for shooter in shooters! {
            let val: Double = Double(arc4random_uniform(400)) / 400 + 0.6
            shooter.animationDuration=val
            shooter.animationRepeatCount=0
            shooter.animationImages=shooterAnimBase
            shooter.startAnimating()
        }
        
        menuController = self.storyboard?.instantiateViewControllerWithIdentifier("Menu") as? MenuController
        if menuController != nil {
            self.navigationController?.pushViewController(menuController!, animated: true)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if menuController != nil {
            if let players = menuController?.modeControl?.selectedSegmentIndex {
                if let begins = menuController?.beginControl?.selectedSegmentIndex {
                    isSecondPlayer = players == 1
                    isSecondTurns = begins == 1
                    mincols[0] = 0
                    mincols[1] = 0
                    mincols[2] = 0
                    
                    for robotLine in robots {
                        for robot in robotLine {
                            robot.hidden = false
                            robot.selected = false
                            robot.userInteractionEnabled = true
                            let val: Double = Double(arc4random_uniform(400)) / 400 + 0.6
                            robot.imageView?.animationDuration=val
                            robot.imageView?.animationRepeatCount=0
                            robot.imageView?.animationImages=redAnimBase
                            robot.imageView?.startAnimating()
                        }
                    }
                    
                    turn()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        for robotLine in robots {
            for var i = 0; i < robotLine.count; i++  {
                robotLine[i].selected = false
                if robotLine[i].focused {
                    for var j = 0; j < i; j++ {
                        robotLine[j].selected = true
                    }
                }
            }
        }
    }
    
    // MARK: - Game logic
    
    @IBAction func shoot(sender: UIButton) {
        path.row = sender.tag / 10 - 1
        path.maxcol = sender.tag % 10 - 1
        path.col = mincols[path.row]
        print("SHOOT: \(path.row), \(path.col), \(path.maxcol)")
        isLastShoot = path.col == path.maxcol
        isFirstShoot = true
        bomb()
    }
    
    func turn() {
        if isSecondTurns {
            print("Second Turn")
            helpLabel?.textColor = UIColor(netHex: 0xcd0000)
            if isSecondPlayer {
                helpLabel?.text = "Player 2 turns, select the droids you want to destroy and push!"
            } else {
                helpLabel?.text = "It's time to Apple TV's turn."
                path.row = 0
                path.col = 0
                path.maxcol = 0
                let rnd = arc4random_uniform(10)
                print("RND: \(rnd)")
                if rnd > 6 {
                    //Mistake of iOS
                    repeat {
                        path.row = Int(arc4random_uniform(3))
                        print("Loop2: \(path.row)")
                    } while (mincols[path.row] >= path.row * 2 + 3)
                    let remained = UInt32((path.row * 2 + 3) - mincols[path.row])
                    path.maxcol=Int(arc4random_uniform(remained)) + mincols[path.row]
                    path.col=mincols[path.row]
                } else {
                    //Correct logic
                    var isShoot = false
                    for var row = 0; row < 3; row++ {
                        for var i = 0; i < 3; i++ {
                            counter[i] = (i * 2 + 3) - mincols[i]
                        }
                        let c = counter[row]
                        if c != 0 {
                            counter[row]--
                            if !isShoot && testShoot() {
                                path.row = row
                                path.col = mincols[row]
                                path.maxcol = path.col + (((row * 2 + 3) - mincols[row]) - counter[row]) - 1
                                isShoot = true
                            }
                        }
                    }
                    if !isShoot {
                        //Random shoot
                        repeat {
                            path.row = Int(arc4random_uniform(3))
                            print("Loop: \(path.row)")
                        } while mincols[path.row] >= path.row * 2 + 3
                        let remained = (path.row * 2 + 3) - mincols[path.row]
                        path.maxcol=Int(arc4random_uniform(UInt32(remained))) + mincols[path.row]
                        path.col = mincols[path.row]
                    }
                }
                isLastShoot = path.col == path.maxcol
                isFirstShoot = true
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.bomb()
                })
            }
        } else {
            print("First Turn")
            helpLabel?.textColor = UIColor(netHex: 0xde8c16)
            helpLabel?.text = "Player 1 turns, select the droids you want to destroy and push!"
        }
    }
    
    func testShoot() -> Bool {
        var a = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        var s = [0, 0, 0]
        for var i = 0; i < 3; i++ {
            let remained = counter[i]
            a[i][0] = remained / 4
            a[i][1] = remained % 4 / 2
            a[i][2] = remained % 2
        }
        for var i = 0; i < 3; i++ {
            s[i]=a[0][i] + a[1][i] + a[2][i]
        }
        let isGood = s[0] % 2 != 1 && s[1] % 2 != 1 && s[2] % 2 != 1
        return isGood;
    }
    
    func bomb() {
        //Disable buttons
        for robotLine in robots {
            for robot in robotLine {
                robot.userInteractionEnabled = false
            }
        }
        
        //Start shooter animation
        let shooter = shooters![path.row]
        if !isLastShoot {
            shooter.image = UIImage(named: "Shooter0093")
        }
        if isFirstShoot {
            shooter.animationImages = shooterAnimBumm
            shooter.animationDuration = 1
        } else {
            shooter.animationImages = shooterAnimShortBumm
            shooter.animationDuration = 0.2
        }
        shooter.animationRepeatCount = 1
        shooter.startAnimating()
        self.performSelector("startLaser", withObject: nil, afterDelay: isFirstShoot ? 1 : 0.2)
    }
    
    func startLaser() {
        player?.currentTime = 0
        player?.play()
        isFirstShoot = false
        let ys = [197, 526, 853]
        laserView?.y = CGFloat(ys[path.row])
        laserView?.x = 240
        laserView?.hidden = false
        let robot = robots[path.row][path.col]
        let duration = 0.15 + (Double(2 - path.row) * 0.1) + (Double(path.col) * 0.05)

        UIView.animateWithDuration(
            duration,
            delay: 0,
            options: .CurveLinear,
            animations: { () -> Void in
                self.laserView?.x = robot.x
            }) { (finished) -> Void in
                self.laserView?.hidden = true
                robot.imageView?.animationDuration = 1.0
                robot.imageView?.animationRepeatCount = 1
                robot.imageView?.animationImages = self.redAnimBumm
                robot.imageView?.startAnimating()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    //self.endAnimation()
                    self.performSelector("endAnimation", withObject: nil, afterDelay: 0.95)
                })
        }
        
        if isLastShoot {
            let shooter = shooters![path.row]
            shooter.image = UIImage(named: "Shooter0001")
            shooter.stopAnimating()
            shooter.animationImages = shooterAnimDown
            shooter.animationDuration = 0.5
            shooter.animationRepeatCount = 1
            shooter.startAnimating()
            self.performSelector("shooterDidDown", withObject: nil, afterDelay: 0.5)
        }
    }
    
    func shooterDidDown() {
        if isLastShoot {
            setShooterAnim(shooters![path.row])
        }
    }
    
    func setShooterAnim(shooter: UIImageView) {
        let val: Double = Double(arc4random_uniform(400)) / 400 + 0.6
        shooter.animationDuration=val
        shooter.animationRepeatCount=0
        shooter.animationImages=shooterAnimBase
        shooter.startAnimating()
    }
    
    func endAnimation() {

        //Hide robot
        print("\(path.row), \(path.col), \(path.maxcol)")
        let robot = robots[path.row][path.col]
        print(robot.tag)
        robot.hidden = true
        
        //Enable buttons and return
        mincols[path.row] = path.col + 1;
        
        if path.col == path.maxcol {
            //end of line
            print("End of Line")

            for robotLine in robots {
                for robot in robotLine {
                    robot.selected = false
                    robot.userInteractionEnabled = true
                }
            }
            setNeedsFocusUpdate()
            
            if mincols[0] + mincols[1] + mincols[2] == 15 {
                //end of game
                var result = ""
                if !isSecondTurns {
                    if isSecondPlayer {
                        result = "PLAYER 1 WON!"
                    } else {
                        result = "YOU WON!"
                    }
                } else {
                    if isSecondPlayer {
                        result = "PLAYER 2 WON!"
                    } else {
                        result = "YOU LOST!"
                    }
                }
                
                //Show result
                resultLabel1?.text = result
                resultLabel2?.text = result
                resultView?.hidden = false
                
            } else {
                //next turn
                print("Next Turn")
                isSecondTurns = !isSecondTurns
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.turn()
                })
            }
        } else {
            //Next shoot
            print("Next Shoot")
            path.col++
            if path.col == path.maxcol {
                isLastShoot = true
            }
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.bomb()
            })
        }

    }
    
    @IBAction func newGame(sender: UIButton) {
        resultView?.hidden = true
        navigationController?.pushViewController(menuController!, animated: true)
    }
    
    // MARK: - Animations
    
    func setBaseAnim() {
        
        redAnimBase = [
            UIImage(named: "Robot0000")!,
            UIImage(named: "Robot0004")!,
            UIImage(named: "Robot0008")!,
            UIImage(named: "Robot0012")!,
            UIImage(named: "Robot0016")!,
            UIImage(named: "Robot0020")!,
            UIImage(named: "Robot0024")!,
            UIImage(named: "Robot0028")!,
            UIImage(named: "Robot0036")!,
        ]
        
        shooterAnimBase = [
            UIImage(named: "Shooter0001")!,
            UIImage(named: "Shooter0005")!,
            UIImage(named: "Shooter0009")!,
            UIImage(named: "Shooter0013")!,
            UIImage(named: "Shooter0017")!,
            UIImage(named: "Shooter0021")!,
            UIImage(named: "Shooter0025")!,
            UIImage(named: "Shooter0029")!,
            UIImage(named: "Shooter0033")!,
            UIImage(named: "Shooter0037")!,
        ]
        
        redAnimBumm = [
            UIImage(named: "Robot0040")!,
            UIImage(named: "Robot0044")!,
            UIImage(named: "Robot0048")!,
            UIImage(named: "Robot0052")!,
            UIImage(named: "Robot0056")!,
            UIImage(named: "Robot0060")!,
            UIImage(named: "Robot0064")!,
            UIImage(named: "Robot0068")!,
            UIImage(named: "Robot0072")!,
            UIImage(named: "Robot0076")!,
            UIImage(named: "Robot0080")!,
            UIImage(named: "Robot0084")!,
            UIImage(named: "Robot0088")!,
            UIImage(named: "Robot0092")!,
            UIImage(named: "Robot0096")!,
            UIImage(named: "Robot0100")!,
            UIImage(named: "Robot0104")!,
        ]

        shooterAnimBumm = [
            UIImage(named: "Shooter0041")!,
            UIImage(named: "Shooter0045")!,
            UIImage(named: "Shooter0049")!,
            UIImage(named: "Shooter0053")!,
            UIImage(named: "Shooter0057")!,
            UIImage(named: "Shooter0061")!,
            UIImage(named: "Shooter0065")!,
            UIImage(named: "Shooter0069")!,
            UIImage(named: "Shooter0073")!,
            UIImage(named: "Shooter0077")!,
            UIImage(named: "Shooter0081")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0093")!,
        ]

        shooterAnimDown = [
            UIImage(named: "Shooter0057")!,
            UIImage(named: "Shooter0049")!,
            UIImage(named: "Shooter0041")!,
        ]
        
        shooterAnimShortBumm = [
            UIImage(named: "Shooter0093")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0085")!,
            UIImage(named: "Shooter0089")!,
            UIImage(named: "Shooter0093")!,
        ]
        
    }
}

class Path: NSObject {
    var row = 0
    var col = 0
    var maxcol = 0
}

