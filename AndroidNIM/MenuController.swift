//
//  MenuController.swift
//  AndroidNIM
//
//  Created by Gabriel Archer on 25/09/2015.
//  Copyright © 2015 swGuru Ltd. All rights reserved.
//

import UIKit

class MenuController: UIViewController {
    
    @IBOutlet var modeControl: UISegmentedControl?
    @IBOutlet var beginControl: UISegmentedControl?
    @IBOutlet var startButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - User interactions
    
    @IBAction func setMode(control: UISegmentedControl) {
        if control.selectedSegmentIndex == 0 {
            beginControl?.setTitle("Apple TV", forSegmentAtIndex: 1)
        } else {
            beginControl?.setTitle("Player 2", forSegmentAtIndex: 1)
        }
    }

    @IBAction func startGame(button: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
